import { wsMessageStore } from "./stores"
type OnMessageFunc = ((this: WebSocket, ev: MessageEvent) => any) | null
type OnErrorFunc = ((this: WebSocket, ev: Event) => any) | null
type OnCloseFunc = ((this: WebSocket, ev: CloseEvent) => any) | null
type OnOpenFunc = ((this: WebSocket, ev: Event) => any) | null

export type WebSocketMessage = {
  action?: string
  data?: any
}

export class WebSocketWrapper {
  private webSocket: WebSocket
  private onMessage: OnMessageFunc
  private onError: OnErrorFunc
  private onClose: OnCloseFunc
  private onOpen: OnOpenFunc

  constructor({ onMessage, onOpen, onClose, onError }: { onMessage?: OnMessageFunc, onOpen?: OnOpenFunc, onClose?: OnCloseFunc, onError?: OnErrorFunc }) {
    this.onOpen = onOpen || this.onOpenDefault
    this.onMessage = onMessage || this.onMessageDefault
    this.onClose = onClose || this.onCloseDefault
    this.onError = onError || this.onErrorDefault
  }

  open() {// @ts-ignore
    const apiUrl = __myapp.env.API_URL;

    this.webSocket = new WebSocket(`ws://${apiUrl}/ws`)
    this.webSocket.onmessage = this.onMessage
    this.webSocket.onopen = this.onOpen
    this.webSocket.onclose = this.onClose
    this.webSocket.onerror = this.onError
  }

  close(code?: number, reason?: string) {
    this.webSocket.close(code, reason)
  }

  send(message: WebSocketMessage) {
    console.log(`[send] `, message);
    this.webSocket.send(JSON.stringify(message))
  }

  onMessageDefault(event: MessageEvent) {
    console.log(`[message] `, event.data);
    wsMessageStore.set(JSON.parse(event.data))
  }

  onOpenDefault(event: Event) {
    console.log(`[open] ${event}`);
  }

  onCloseDefault(event: CloseEvent) {
    if (event.wasClean) {
      console.log(
        `[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`
      );
    } else {
      console.log("[close] Connection died");
    }
  }

  onErrorDefault(event: ErrorEvent) {
    console.error(`[error] ${event}`);
  }
}